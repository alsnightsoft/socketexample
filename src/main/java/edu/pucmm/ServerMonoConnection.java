package edu.pucmm;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerMonoConnection {

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(3000);
            serverSocket.setSoTimeout(5 * 1000);
            Socket socket = serverSocket.accept();
            PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            System.out.println("Recibo del socket: " + bufferedReader.readLine());
            String toSend = "Hello client!";
            printWriter.println(toSend);
        } catch (Exception ignored) {
        }
    }
}
