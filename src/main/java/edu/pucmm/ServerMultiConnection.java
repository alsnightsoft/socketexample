package edu.pucmm;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerMultiConnection {

    public static void main(String[] args) {
        try {
            ExecutorService executorService = Executors.newFixedThreadPool(10);
            ServerSocket serverSocket = new ServerSocket(3000);
            serverSocket.setSoTimeout(5 * 1000);
            while (!serverSocket.isClosed()) {
                Socket socket = serverSocket.accept();
                executorService.submit(new MyClient(socket));
            }
            System.out.println("Close server");
        } catch (Exception ignored) {
        }
    }

    private static class MyClient implements Callable<Boolean> {

        private Socket socket;

        MyClient(Socket socket) {
            this.socket = socket;
        }

        @Override
        public Boolean call() throws Exception {
            try {
                socket.setSoTimeout(5 * 1000);
                PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                System.out.println("Recibo del socket: " + bufferedReader.readLine());
                String toSend = "Hello client!";
                printWriter.println(toSend);
            } catch (Exception ignored) {
            }
            return true;
        }
    }
}
