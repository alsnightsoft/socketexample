package edu.pucmm;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {

    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost", 3000);
            socket.setSoTimeout(5 * 1000);
            PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String toSend = "Hello server";
            printWriter.println(toSend);
            System.out.println("Server send me: " + bufferedReader.readLine());
        } catch (Exception ignored) {
        }
    }
}
